﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EasyEstimation.Entities;

namespace EasyEstimation.Services.Abstracts
{
   public interface IUserManager
    {
        Task<IEnumerable<User>> GetAllAsync();
        Task<User> GetAsync(int id);
        Task<User> CreateAsync(User model);
        Task<User> EditAsync(User model);
        Task<bool> DeleteAsync(int id);
    }
}
