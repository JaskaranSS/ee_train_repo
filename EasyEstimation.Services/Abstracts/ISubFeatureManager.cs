﻿using System.Collections.Generic;
using System.Threading.Tasks;
using EasyEstimation.Entities;

namespace EasyEstimation.Services.Abstracts
{
    public interface ISubFeatureManager
    {
        Task<IEnumerable<SubFeature>> GetAllAsync();
        Task<SubFeature> GetAsync(int id);
        Task<SubFeature> CreateAsync(SubFeature model);
        Task<SubFeature> EditAsync(SubFeature model);
        Task<bool> DeleteAsync(int id);
    }
}
