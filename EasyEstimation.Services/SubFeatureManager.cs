﻿using AutoMapper;
using EasyEstimation.Data;
using EasyEstimation.Entities;
using EasyEstimation.Services.Abstracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasyEstimation.Services
{
    /// <summary>
    /// Manager for subfeatures 
    /// handles add ,update, delete of sub features
    /// </summary>
    public class SubFeatureManager : ISubFeatureManager
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public SubFeatureManager(ILogger<SubFeatureManager> logger,
                           IUnitOfWork unitOfWork,
                           IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        /// <summary>
        /// Gets all the sub features present in the database
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<SubFeature>> GetAllAsync()
        {
            var SubFeatures = await _unitOfWork.SubFeatureRepository.GetListAsync(x => x.IsDeleted == false);
            return _mapper.Map<IEnumerable<SubFeature>>(SubFeatures);
        }
        /// <summary>
        /// Gets the sub feature by taking id of the sub feature as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SubFeature> GetAsync(int id)
        {
            var SubFeature = await _unitOfWork.SubFeatureRepository.GetAsync(x => x.Id == id && x.IsDeleted == false);
            return _mapper.Map<SubFeature>(SubFeature);
        }
        /// <summary>
        /// Adds sub feature to the database also only allows uniquely named sub features 
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<SubFeature> CreateAsync(SubFeature model)
        {
            Data.Entity.SubFeature SubFeature;
            try
            {
                SubFeature = _mapper.Map<Data.Entity.SubFeature>(model);
                await _unitOfWork.SubFeatureRepository.AddAsync(SubFeature);
                await _unitOfWork.CommitAsync();

                return _mapper.Map<SubFeature>(SubFeature);
                    
                
                //var SubFeature = _mapper.Map<Data.Entity.SubFeature>(model);
                //await _unitOfWork.SubFeatureRepository.AddAsync(SubFeature);
                //await _unitOfWork.CommitAsync();

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to add SubFeature");
                return null;
            }
        }
        /// <summary>
        /// Gets the sub features from the user and updates the existing one in the database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<SubFeature> EditAsync(SubFeature model)
        {
            var SubFeature = await _unitOfWork.SubFeatureRepository.GetAsync(model.Id);
            if (SubFeature == null)
            {
                _logger.LogDebug("SubFeature not found at time of updating");
                throw new ArgumentException("SubFeature has been deleted.");
            }

            _mapper.Map(model, SubFeature);

            _unitOfWork.SubFeatureRepository.Update(SubFeature);

            await _unitOfWork.CommitAsync();

            return _mapper.Map<SubFeature>(SubFeature);
        }
        /// <summary>
        /// Removes the sub feature from the database by taking the id of the subfeature as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                var SubFeature = await _unitOfWork.SubFeatureRepository.GetAsync(id);
                if (SubFeature == null)
                {
                    _logger.LogDebug("SubFeature not found at time of deleting");
                    throw new ArgumentException("SubFeature has been deleted.");
                }
                await _unitOfWork.SubFeatureRepository.DeleteAsync(id);
                await _unitOfWork.CommitAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to delete SubFeature");
                return false;
            }
        }
    }
}
