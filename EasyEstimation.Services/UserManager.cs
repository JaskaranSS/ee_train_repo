﻿using AutoMapper;
using EasyEstimation.Data;
using EasyEstimation.Entities;
using EasyEstimation.Services.Abstracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasyEstimation.Services
{
    public class UserManager : IUserManager
    {
        private readonly ILogger _logger;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public UserManager(ILogger<UserManager> logger,
                           IUnitOfWork unitOfWork,
                           IMapper mapper)
        {
            _logger = logger;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }
        public async Task<IEnumerable<User>> GetAllAsync()
        {
            var users = await _unitOfWork.UserRepository.GetListAsync(x => x.IsDeleted == false);
            return _mapper.Map<IEnumerable<User>>(users);
        }

        public async Task<User> GetAsync(int id)
        {
            var user = await _unitOfWork.UserRepository.GetAsync(x => x.Id == id && x.IsDeleted == false);
            return _mapper.Map<User>(user);
        }

        public async Task<User> CreateAsync(User model)
        {
            try
            {
                if (await _unitOfWork.UserRepository.IsNameExistAsync(model.Username))
                {
                    throw new ArgumentException("user name alredy exist, please try another one");
                }
                var user = _mapper.Map<Data.Entity.User>(model);
                await _unitOfWork.UserRepository.AddAsync(user);
                await _unitOfWork.CommitAsync();

                return _mapper.Map<User>(user);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to add user");
                return null;
            }
        }

        public async Task<User> EditAsync(User model)
        {
            var user = await _unitOfWork.UserRepository.GetAsync(model.Id);
            if (user == null)
            {
                _logger.LogDebug("User not found at time of updating");
                throw new ArgumentException("User has been deleted.");
            }

            if (await _unitOfWork.UserRepository.IsNameExistExcludingIdAsync(model.Id, model.Username))
            {
                throw new ArgumentException("User name alredy exist, please try another one");
            }

            _mapper.Map(model, user);

            _unitOfWork.UserRepository.Update(user);

            await _unitOfWork.CommitAsync();

            return _mapper.Map<User>(user);
        }

        public async Task<bool> DeleteAsync(int id)
        {
            try
            {
                var user = await _unitOfWork.UserRepository.GetAsync(id);
                if (user == null)
                {
                    _logger.LogDebug("User not found at time of deleting");
                    throw new ArgumentException("User has been deleted.");
                }
                await _unitOfWork.UserRepository.DeleteAsync(id);
                await _unitOfWork.CommitAsync();
                return true;
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Unable to delete user");
                return false;
            }
        }
    }
}
