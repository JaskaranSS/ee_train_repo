﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class UserEstimationConfiguration : BaseEntityTypeConfiguration<UserEstimation, int>
    {
        public override void Config(EntityTypeBuilder<UserEstimation> builder)
        {
            builder.ToTable("UserEstimations");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Email).IsRequired();
            builder.Property(x => x.Mobile).IsRequired();
            builder.Property(x => x.OrganisationName).IsRequired();


        }
    }
}
