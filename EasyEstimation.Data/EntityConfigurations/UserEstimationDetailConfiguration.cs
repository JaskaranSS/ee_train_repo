﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class UserEstimationDetailConfiguration : BaseEntityTypeConfiguration<UserEstimationDetail, int>
    {
        public override void Config(EntityTypeBuilder<UserEstimationDetail> builder)
        {
            builder.ToTable("UserEstimationDetails");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

        }
    }
}
