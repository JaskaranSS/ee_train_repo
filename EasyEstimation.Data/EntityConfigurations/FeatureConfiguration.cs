﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class FeatureConfiguration : BaseEntityTypeConfiguration<Feature, int>
    {
        public override void Config(EntityTypeBuilder<Feature> builder)
        {
            builder.ToTable("Features");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Description).IsRequired();

            builder.HasMany(x => x.CategoryFeatures)
                 .WithOne(x => x.Feature)
                 .HasPrincipalKey(x => x.Id)
                 .HasForeignKey(x => x.FeatureId);
        }
    }
}
