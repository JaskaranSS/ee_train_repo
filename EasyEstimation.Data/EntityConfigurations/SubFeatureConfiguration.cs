﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class SubFeatureConfiguration : BaseEntityTypeConfiguration<SubFeature, int>
    {
        public override void Config(EntityTypeBuilder<SubFeature> builder)
        {
            builder.ToTable("SubFeature");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Description).IsRequired().HasMaxLength(300); 
            //builder.Property(x => x.ImageId).IsRequired();
        }
    }
}