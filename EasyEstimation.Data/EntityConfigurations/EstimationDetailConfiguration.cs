﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class EstimationDetailConfiguration : BaseEntityTypeConfiguration<EstimationDetail, int>
    {
        public override void Config(EntityTypeBuilder<EstimationDetail> builder)
        {
            builder.ToTable("EstimationDetails");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.Hour).IsRequired();
            
        }
    }
}
