﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class CategoryConfiguration : BaseEntityTypeConfiguration<Category, int>
    {
        public override void Config(EntityTypeBuilder<Category> builder)
        {
            builder.ToTable("Categories");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();

            builder.Property(x => x.Name).IsRequired();

            builder.HasMany(x => x.CategoryFeatures)
                  .WithOne(x => x.Category)
                  .HasPrincipalKey(x => x.Id)
                  .HasForeignKey(x => x.CategoryId);
        }
    }
}
