﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class PricingDetailConfiguration : BaseEntityTypeConfiguration<PricingDetail, int>
    {
        public override void Config(EntityTypeBuilder<PricingDetail> builder)
        {
            builder.ToTable("PricingDetails");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Rate).IsRequired();
        }
    }
}
