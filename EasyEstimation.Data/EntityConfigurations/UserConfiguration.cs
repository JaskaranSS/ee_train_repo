﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class UserConfiguration : BaseEntityTypeConfiguration<User, int>
    {
        public override void Config(EntityTypeBuilder<User> builder)
        {
            builder.ToTable("Users");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
        }
    }
}
