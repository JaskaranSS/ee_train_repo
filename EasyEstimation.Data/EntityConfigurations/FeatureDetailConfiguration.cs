﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class FeatureDetailConfiguration : BaseEntityTypeConfiguration<FeatureDetail, int>
    {
        public override void Config(EntityTypeBuilder<FeatureDetail> builder)
        {
            builder.ToTable("FeatureDetails");

            builder.HasKey(x => x.Id);

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.Property(x => x.Name).IsRequired();
            builder.Property(x => x.Description).IsRequired();
            builder.Property(x => x.Icon).IsRequired();
        }
    }
}
