﻿using EasyEstimation.Data.Entity;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace EasyEstimation.Data.EntityConfigurations
{
    internal class CategoryFeatureConfiguration : BaseEntityTypeConfiguration<CategoryFeature, int>
    {
        public override void Config(EntityTypeBuilder<CategoryFeature> builder)
        {
            builder.ToTable("CategoriesFeatures");

            builder.Property(x => x.Id).ValueGeneratedOnAdd();
            builder.HasKey(x => new { x.CategoryId, x.FeatureId });

            builder.HasOne(x => x.Category)
                   .WithMany(x => x.CategoryFeatures)
                   .HasPrincipalKey(x => x.Id)
                   .HasForeignKey(x => x.CategoryId);

            builder.HasOne(x => x.Feature)
                   .WithMany(x => x.CategoryFeatures)
                   .HasPrincipalKey(x => x.Id)
                   .HasForeignKey(x => x.FeatureId);
        }
    }
}
