﻿using EasyEstimation.Data.CoreData;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace EasyEstimation.Data
{
    public class Repository<TEntity, TKey> : IRepository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        protected readonly DbContext dbContext;

        public Repository(DbContext dbContext)
        {
            this.dbContext = dbContext;
        }

        public async Task<TEntity> AddAsync(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("Entity");
            }
            entity.CreatedDate = DateTime.Now;
            await dbContext.Set<TEntity>().AddAsync(entity);
            return entity;
        }
        public async Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            if (entities == null || !entities.Any())
            {
                return;
            }

            foreach (var entity in entities)
            {
                await AddAsync(entity);
            }
        }

        public void Delete(TKey id)
        {
            TEntity entity = dbContext.Set<TEntity>().FirstOrDefault(x => x.Id.Equals(id));
            if (entity == null)
            {
                return;
            }

            Delete(entity);
        }

        public async Task DeleteAsync(TKey id)
        {
            TEntity entity = await dbContext.Set<TEntity>().FirstOrDefaultAsync(x => x.Id.Equals(id));
            if (entity == null)
            {
                return;
            }
            Delete(entity);
        }

        public void Delete(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("Entity");
            }

            entity.IsDeleted = true;
            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void DeleteRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Delete(entity);
            }
        }

        public void Update(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("Entity");
            }

            dbContext.Entry(entity).State = EntityState.Modified;
        }

        public void UpdateRange(IEnumerable<TEntity> entities)
        {
            foreach (var entity in entities)
            {
                Update(entity);
            }
        }

        public void Remove(TEntity entity)
        {
            if (entity == null)
            {
                throw new ArgumentNullException("Entity");
            }

            dbContext.Set<TEntity>().Remove(entity);
        }

        public void RemoveRange(IEnumerable<TEntity> entities)
        {
            dbContext.Set<TEntity>().RemoveRange(entities);
        }

        public IQueryable<TEntity> GetQueryable(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return this.GetQueryablePropertyIncluded(null, includeProperties).AsQueryable<TEntity>();
        }

        public IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return this.GetQueryablePropertyIncluded(predicate, includeProperties);
        }

        public IEnumerable<TEntity> GetList(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return this.GetQueryablePropertyIncluded(null, includeProperties).AsEnumerable<TEntity>();
        }

        public IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return this.GetQueryablePropertyIncluded(predicate, includeProperties);
        }

        public async Task<TEntity> GetAsync(TKey id, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return await this.GetQueryablePropertyIncluded(null, includeProperties).FirstOrDefaultAsync(x => x.Id.Equals(id));
        }

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return await this.GetQueryablePropertyIncluded(predicate, includeProperties).FirstOrDefaultAsync(predicate);
        }

        public async Task<IEnumerable<TEntity>> GetListAsync(params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return await this.GetQueryablePropertyIncluded(null, includeProperties).ToListAsync();
        }

        public async Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            return await this.GetQueryablePropertyIncluded(predicate, includeProperties).ToListAsync();
        }

        public async Task<bool> AnyAsync()
        {
            return await dbContext.Set<TEntity>().AnyAsync();
        }

        public async Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await dbContext.Set<TEntity>().AnyAsync(predicate);
        }

        #region Private Functions

        private IQueryable<TEntity> GetQueryablePropertyIncluded(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties)
        {
            IQueryable<TEntity> entities = dbContext.Set<TEntity>();

            if (predicate != null)
            {
                entities = entities.Where(predicate);
            }

            foreach (Expression<Func<TEntity, object>> includeProperty in includeProperties)
            {
                entities = entities.Include(includeProperty);
            }

            return entities;
        }


        #endregion
    }
}
