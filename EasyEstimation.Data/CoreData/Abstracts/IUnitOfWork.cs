﻿using EasyEstimation.Data.CoreData.Abstracts;
using EasyEstimation.Data.Repositories.Abstracts;

namespace EasyEstimation.Data
{
    public interface IUnitOfWork : IBaseUnitOfWork
    {
        public IUserRepository UserRepository { get; }

        public ISubFeatureRepository SubFeatureRepository { get; }
        
    }
}
