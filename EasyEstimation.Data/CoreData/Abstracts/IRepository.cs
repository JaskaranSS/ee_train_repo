﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace EasyEstimation.Data.CoreData
{
    public interface IRepository<TEntity, TKey> where TEntity : class
    {
        Task<TEntity> AddAsync(TEntity entity);
        Task AddRangeAsync(IEnumerable<TEntity> entities);

        void Update(TEntity entity);
        void UpdateRange(IEnumerable<TEntity> entities);
        /// <summary>
        /// It soft deletion the entity with <see cref="IEntity{TKey}.Id"/> by marking <see cref="IEntity{TKey}.IsDeleted"/> property to <see cref="bool.TrueString"/>
        /// </summary>
        /// <param name="id"></param>
        void Delete(TKey id);
        Task DeleteAsync(TKey id);
        /// <summary>
        /// It soft deletion the entity by marking <see cref="IEntity{TKey}.IsDeleted"/> property to <see cref="bool.TrueString"/>
        /// </summary>
        /// <param name="entity"></param>
        void Delete(TEntity entity);
        /// <summary>
        /// It soft deletion the enities by marking <see cref="IEntity{TKey}.IsDeleted"/> property to <see cref="bool.TrueString"/>
        /// </summary>
        /// <param name="entities"></param>
        void DeleteRange(IEnumerable<TEntity> entities);
        /// <summary>
        /// It removes the entity permanently, should be used wisely.
        /// 
        /// Review the use case and use <see cref="IRepository{TEntity, TKey}.Delete(TEntity)" /> method.
        /// </summary>
        /// <param name="entity"></param>
        void Remove(TEntity entity);

        /// <summary>
        /// It removes the entity permanently, should be used wisely.
        /// 
        /// Review the use case and use <see cref="IRepository{TEntity, TKey}.DeleteRange(TEntity)" /> method.
        /// </summary>
        /// <param name="entity"></param>
        void RemoveRange(IEnumerable<TEntity> entities);
        IQueryable<TEntity> GetQueryable(params Expression<Func<TEntity, object>>[] includeProperties);
        IQueryable<TEntity> GetQueryable(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);
        IEnumerable<TEntity> GetList(params Expression<Func<TEntity, object>>[] includeProperties);
        IEnumerable<TEntity> GetList(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);
        Task<IEnumerable<TEntity>> GetListAsync(params Expression<Func<TEntity, object>>[] includeProperties);
        Task<IEnumerable<TEntity>> GetListAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);
        Task<TEntity> GetAsync(TKey id, params Expression<Func<TEntity, object>>[] includeProperties);
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate, params Expression<Func<TEntity, object>>[] includeProperties);

        Task<bool> AnyAsync();
        Task<bool> AnyAsync(Expression<Func<TEntity, bool>> predicate);

    }
}
