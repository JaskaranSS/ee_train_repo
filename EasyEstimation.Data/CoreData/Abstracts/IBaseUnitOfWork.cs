﻿using System;
using System.Threading.Tasks;

namespace EasyEstimation.Data.CoreData.Abstracts
{
    public interface IBaseUnitOfWork : IDisposable
    {
        Task CommitAsync();
    }
}
