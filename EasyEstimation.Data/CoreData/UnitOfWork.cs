﻿using EasyEstimation.Data.Repositories;
using EasyEstimation.Data.Repositories.Abstracts;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;

namespace EasyEstimation.Data
{
    //Following microsoft docs 
    public class UnitOfWork : IUnitOfWork
    {
        private readonly ApplicationDbContext _dbContext;
        private readonly ILogger _logger;


        public UnitOfWork(
            ILogger<UnitOfWork> logger,
            ApplicationDbContext context,
            IUserRepository userRepository,
            ISubFeatureRepository subFeatureRepository
            
            )
        {
            _logger = logger;
            _dbContext = context;

            UserRepository = userRepository;
            SubFeatureRepository = subFeatureRepository;
            
        }
        public IUserRepository UserRepository { get; }
        public ISubFeatureRepository SubFeatureRepository { get; }
        public async Task CommitAsync()
        {
            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception ex)
            {
                _logger.LogError(ex, "Exception occured on SaveChanges.");
                throw;
            }
        }

        void IDisposable.Dispose()
        {
            if (_dbContext != null)
            {
                _dbContext.Dispose();
            }
        }
    }
}
