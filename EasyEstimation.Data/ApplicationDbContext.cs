﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace EasyEstimation.Data
{
    public class ApplicationDbContext : DbContext
    {

        public ApplicationDbContext(DbContextOptions options) : base(options)
        {

        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfigurationsFromAssembly(GetType().Assembly);
            base.OnModelCreating(modelBuilder);
        }

        public async override Task<int> SaveChangesAsync(CancellationToken cancellationToken = default)
        {
            OnBeforeSaveChanges();
            var result = await base.SaveChangesAsync(cancellationToken);
            await OnAfterSaveChangesAsync();
            return result;
        }

        private void OnBeforeSaveChanges()
        {
            base.ChangeTracker.DetectChanges();

            var now = DateTime.UtcNow;

            List<string> excludedProperties = new List<string>() { "CreatedDate", "UpdatedDate", "CreatedBy", "UpdatedBy" };

            foreach (EntityEntry entry in base.ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
                {
                    continue;
                }

                if (entry.State == EntityState.Added || entry.Metadata.FindProperty("CreatedDate") != null)
                {
                    entry.Property("CreatedDate").CurrentValue = now;
                    entry.Property("UpdatedDate").CurrentValue = now;
                    //  entry.Property("CreatedBy").CurrentValue = userId;
                    //  entry.Property("UpdatedBy").CurrentValue = userId;
                }
            }
        }


        private Task OnAfterSaveChangesAsync()
        {
            return base.SaveChangesAsync();
        }
    }
}
