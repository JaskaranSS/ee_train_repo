﻿using EasyEstimation.Data.Entity;
using EasyEstimation.Data.Repositories.Abstracts;
using System.Threading.Tasks;

namespace EasyEstimation.Data.Repositories
{
    public class FeatureRepository : Repository<Feature, int>, IFeatureRepository
    {
        public FeatureRepository(ApplicationDbContext context) : base(context)
        {

        }

    }
}
