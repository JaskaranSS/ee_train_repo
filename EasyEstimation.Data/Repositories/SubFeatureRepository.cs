﻿using EasyEstimation.Data.Entity;
using EasyEstimation.Data.Repositories.Abstracts;
using System.Threading.Tasks;

namespace EasyEstimation.Data.Repositories
{
    public class SubFeatureRepository : Repository<SubFeature, int>, ISubFeatureRepository
    {
        public SubFeatureRepository(ApplicationDbContext context) : base(context)
        {

        }
        

    }
}
