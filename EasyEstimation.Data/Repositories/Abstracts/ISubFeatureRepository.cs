﻿using EasyEstimation.Data.CoreData;
using EasyEstimation.Data.Entity;
using System.Threading.Tasks;

namespace EasyEstimation.Data.Repositories.Abstracts
{
    public interface ISubFeatureRepository : IRepository<SubFeature, int>
    {
        
    }
}
