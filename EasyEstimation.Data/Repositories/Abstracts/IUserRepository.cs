﻿using EasyEstimation.Data.CoreData;
using EasyEstimation.Data.Entity;
using System.Threading.Tasks;

namespace EasyEstimation.Data.Repositories.Abstracts
{
    public interface IUserRepository : IRepository<User,int>
    {
        Task<bool> IsNameExistExcludingIdAsync(int id, string name);
        Task<bool> IsNameExistAsync(string name);
        Task<bool> IsAdminAsync(int id);
    }
}
