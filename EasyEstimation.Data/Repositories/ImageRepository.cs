﻿using EasyEstimation.Data.Entity;
using EasyEstimation.Data.Repositories.Abstracts;
using System.Threading.Tasks;

namespace EasyEstimation.Data.Repositories
{
    public class ImageRepository : Repository<ImageModel, int>, IImageRepository
    {
        public ImageRepository(ApplicationDbContext context) : base(context)
        {

        }
        

    }
}
