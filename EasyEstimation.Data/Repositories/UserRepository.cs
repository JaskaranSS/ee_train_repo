﻿using EasyEstimation.Data.Entity;
using EasyEstimation.Data.Repositories.Abstracts;
using System.Threading.Tasks;

namespace EasyEstimation.Data.Repositories
{
    public class UserRepository : Repository<User, int>, IUserRepository
    {
        public UserRepository(ApplicationDbContext context) : base(context)
        {

        }
        public async Task<bool> IsNameExistExcludingIdAsync(int id, string name)
        {
            return await AnyAsync(x => x.Id != id
                                       && string.Equals(x.Username.ToLower(), name.ToLower())
                                       && !x.IsDeleted);
        }
        public async Task<bool> IsNameExistAsync(string name) 
        {
            return await AnyAsync(x => string.Equals(x.Username.ToLower(), name.ToLower()));
        }

        public async Task<bool> IsAdminAsync(int id)
        {
            return await AnyAsync(x => x.Id != id
                                      && x.IsAdmin
                                      && !x.IsDeleted);
        }
    }
}
