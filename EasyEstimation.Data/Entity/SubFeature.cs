﻿using System.Collections.Generic;

namespace EasyEstimation.Data.Entity
{
    public class SubFeature : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        
    }
}