﻿namespace EasyEstimation.Data.Entity
{
    public class UserEstimationDetail : BaseEntity<int>
    {
        public int UserEstimationId { get; set; }
        public int EstimationDetailId { get; set; }
    }
}
