﻿namespace EasyEstimation.Data.Entity
{
    public class EstimationDetail : BaseEntity<int>
    {
        public int CategoryId { get; set; }
        public int FeatureDetailId { get; set; }
        public int Hour { get; set; }
        public bool IsActive { get; set; }
    }
}
