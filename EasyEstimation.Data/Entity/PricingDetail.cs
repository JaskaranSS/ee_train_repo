﻿namespace EasyEstimation.Data.Entity
{
    public class PricingDetail : BaseEntity<int>
    {
        public int CategoryId { get; set; }
        public double Rate { get; set; }
        public bool IsActive { get; set; }
    }
}
