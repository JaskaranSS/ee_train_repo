﻿using System.Collections.Generic;

namespace EasyEstimation.Data.Entity
{
    public class User : BaseEntity<int>
    {
        public string Username { get; set; }
        public string Email { get; set; }
        public bool IsAdmin { get; set; }
    }
}
