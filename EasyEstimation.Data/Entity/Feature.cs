﻿using System.Collections.Generic;

namespace EasyEstimation.Data.Entity
{
    public class Feature : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<CategoryFeature> CategoryFeatures { get; set; }
    }
}
