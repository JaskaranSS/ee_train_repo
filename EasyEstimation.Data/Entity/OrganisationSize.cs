﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEstimation.Data.Entity
{
    public class OrganisationSize : BaseEntity<int>
    {
        public string Name { get; set; }
    }
}
