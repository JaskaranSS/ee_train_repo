﻿using System.Collections.Generic;

namespace EasyEstimation.Data.Entity
{
    public class Category : BaseEntity<int>
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }

        public virtual ICollection<CategoryFeature> CategoryFeatures { get; set; }
    }
}
