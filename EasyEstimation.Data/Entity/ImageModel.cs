﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyEstimation.Data.Entity
{
    public class ImageModel: BaseEntity<int>
    {
        public string FormFile { get; set; }
    }
}
