﻿using System.Collections.Generic;

namespace EasyEstimation.Data.Entity
{
    public class CategoryFeature : BaseEntity<int>
    {
        public int CategoryId { get; set; }
        public int FeatureId { get; set; }

        public virtual Category Category { get; set; }
        public virtual Feature Feature { get; set; }
    }
}
