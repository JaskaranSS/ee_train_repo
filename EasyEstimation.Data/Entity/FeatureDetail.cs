﻿using System.Collections.Generic;

namespace EasyEstimation.Data.Entity
{
    public class FeatureDetail : BaseEntity<int>
    {
        public string Name { get; set; }
        public int? ParentFeatureDetailId { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }
    }
}
