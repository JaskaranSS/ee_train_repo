﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace EasyEstimation.ImageSaver
{
    static public class ImageSaver
    {
        private static string fileName;

        public static async Task<string> Saver(IFormFile file)
        {

            if (file.Length > 0)
            {
                string fileName = (Convert.ToString(DateTime.Now).Replace(":", "")).Replace("-", "");
                fileName = fileName + Convert.ToString(Path.GetExtension(Path.GetFileName(file.FileName)));
                var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", "SubFeatureImages", fileName); 
                using (FileStream stream = System.IO.File.Create(filePath))
                {
                    await file.CopyToAsync(stream);
                    stream.Flush();
                }
            }
            return fileName;
        }
    }
}
