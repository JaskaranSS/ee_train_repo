﻿using AutoMapper;
using EasyEstimation.API.Models;
using EasyEstimation.Data.Entity;

namespace EasyEstimation.API.Mapping
{
    public class MapperConfiguration : Profile
    {
        public MapperConfiguration()
        {
            #region User Mapper
            CreateMap<User, Entities.User>().ReverseMap();
            CreateMap<Entities.User, UserModel>().ReverseMap();
            #endregion
            #region SubFeature Mapper
            CreateMap<SubFeature, Entities.SubFeature>().ReverseMap();
            CreateMap<Entities.SubFeature, SubFeatureModel>().ReverseMap();
            #endregion

        }
    }
}
