﻿namespace EasyEstimation.API.Resources.Abstracts
{
    /// <summary>
    /// The class respresent IMessageManager
    /// </summary>
    public interface IMessageManager
    {
        /// <summary>
        /// Gets the message based on key
        /// </summary>
        /// <param name="key">message key</param>
        /// <returns></returns>
        string GetMessage(string key);

        /// <summary>
        /// Gets the message based on key and arguments
        /// </summary>
        /// <param name="key">message key</param>
        /// <returns></returns>
        string GetMessage(string key, params object[] args);
    }
}
