﻿using EasyEstimation.API.Resources.Abstracts;
using Microsoft.Extensions.Localization;
using System.Reflection;

namespace EasyEstimation.API.Resources
{
    /// <summary>
    /// The class represents MessageManager for retriving messages
    /// </summary>
    public class MessageManager : IMessageManager
    {
        private readonly IStringLocalizer _localizer;

        /// <summary>
        /// Initializes a new instance of the <see cref="MessageManager"/> class.
        /// </summary>
        /// <param name="localizerFactory">The localizer</param>
        public MessageManager(IStringLocalizerFactory localizerFactory)
        {
            _localizer = localizerFactory.Create(typeof(MessagesResource));

            var type = typeof(MessagesResource);
            var assemblyName = new AssemblyName(type.GetTypeInfo().Assembly.FullName);
            _localizer = localizerFactory.Create("MessagesResource", assemblyName.Name);
        }

        /// <summary>
        /// Gets the message based on key
        /// </summary>
        /// <param name="key">message key</param>
        /// <returns></returns>
        public string GetMessage(string key) => _localizer[key];

        // <summary>
        /// Gets the message based on key and arguments
        /// </summary>
        /// <param name="key">message key</param>
        /// <returns></returns>
        public string GetMessage(string key, params object[] args) => string.Format(_localizer[key], args);
    }
}
