﻿using EasyEstimation.API.Mapping;
using EasyEstimation.API.Resources;
using EasyEstimation.API.Resources.Abstracts;
using EasyEstimation.Data;
using EasyEstimation.Data.Repositories;
using EasyEstimation.Data.Repositories.Abstracts;
using EasyEstimation.Services;
using EasyEstimation.Services.Abstracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace EasyEstimation.API.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructureLayerServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddAutoMapper(typeof(MapperConfiguration));
            services.AddVersioning();
            services.AddContexts(configuration);
            services.ConfigureLocalization();
            services.RegisterServices();            
            services.RegisterRepositories();
            services.RegisterManagers();
            return services;
        }

        private static IServiceCollection AddContexts(this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection");
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(
                                      connectionString,
                                       x => x.MigrationsAssembly("EasyEstimation.Data")
                                       ));
            return services;
        }

        private static IServiceCollection AddVersioning(this IServiceCollection services)
        {
            return services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = new ApiVersion(1, 0);
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
            });
        }

        private static IServiceCollection ConfigureLocalization(this IServiceCollection services)
        {
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.Configure<RequestLocalizationOptions>(
                       options =>
                       {
                           var supportedCultures = new List<CultureInfo>
                               {
                                   new CultureInfo("en-US"),
                               };

                           options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
                           options.SupportedCultures = supportedCultures;
                           options.SupportedUICultures = supportedCultures;
                           options.RequestCultureProviders.Insert(0, new QueryStringRequestCultureProvider());
                       });

            services.AddMvc()
                    .AddViewLocalization()
                    .AddDataAnnotationsLocalization(options =>
                    {
                        options.DataAnnotationLocalizerProvider = (type, factory) =>
                        {
                            var assemblyName = new AssemblyName(typeof(MessagesResource).GetTypeInfo().Assembly.FullName);
                            return factory.Create("MessagesResource", assemblyName.Name);
                        };
                    });

            return services;
        }

        private static IServiceCollection RegisterServices(this IServiceCollection services)
        {
            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<IMessageManager, MessageManager>();
            return services;
        }
        private static IServiceCollection RegisterRepositories(this IServiceCollection services)
        {
            services.AddScoped<IUserRepository, UserRepository>();
            services.AddScoped<ISubFeatureRepository, SubFeatureRepository>();
            return services;
        }
        private static IServiceCollection RegisterManagers(this IServiceCollection services)
        {
            services.AddScoped<IUserManager, UserManager>();
            services.AddScoped<ISubFeatureManager, SubFeatureManager>();
            return services;
        }
    }
}