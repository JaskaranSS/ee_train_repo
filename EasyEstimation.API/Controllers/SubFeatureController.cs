﻿using AutoMapper;
using EasyEstimation.API.Models;
using EasyEstimation.API.Resources;
using EasyEstimation.API.Resources.Abstracts;
using EasyEstimation.Services.Abstracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasyEstimation.API.Controllers
{
    /// <summary>
    /// sub feature controller handles api requests for sub feature database
    /// uses SubfeatureManager service to complete the requests
    /// </summary>
    public class SubFeatureController : BaseController
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly ISubFeatureManager _subFeatureManager;
        private readonly IMessageManager _messageManager;

        public SubFeatureController(ILogger<SubFeatureController> logger, IMapper mapper,
            ISubFeatureManager subFeatureManager, IMessageManager messageManager)
        {
            _logger = logger;
            _mapper = mapper;
            _subFeatureManager = subFeatureManager;
            _messageManager = messageManager;
        }
        /// <summary>
        /// Gets all the subfeature from the database
        /// </summary>
        /// <returns></returns>
        [HttpGet("Get")]
        [ProducesResponseType(typeof(IEnumerable<SubFeatureModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var SubFeatures = await _subFeatureManager.GetAllAsync();

                return Ok(_mapper.Map<IEnumerable<SubFeatureModel>>(SubFeatures));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, _messageManager.GetMessage(MessageKeys.SF_MSG_01));
                return BadRequest(ex.Message);
            }

        }
        /// <summary>
        /// Gets the sub feature by taking id of the sub feature as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ProducesResponseType(typeof(SubFeatureModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return BadRequest(_messageManager.GetMessage(MessageKeys.SF_MSG_02));
            }
            try
            {
                var SubFeature = await _subFeatureManager.GetAsync(id.Value);

                return Ok(_mapper.Map<SubFeatureModel>(SubFeature));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, _messageManager.GetMessage(MessageKeys.SF_MSG_03));
                return BadRequest(ex.Message);
            }

        }
        /// <summary>
        /// Gets the subfeatures from the enduser and updates the existing one in the database
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPut("Put")]
        [ProducesResponseType(typeof(SubFeatureModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> Put([FromBody] SubFeatureModel model)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var detail = await _subFeatureManager.EditAsync(_mapper.Map<Entities.SubFeature>(model));
                return Ok(_mapper.Map<SubFeatureModel>(detail));
            }
            catch (Exception ex)
            {
                _logger.LogError(_messageManager.GetMessage(MessageKeys.SF_MSG_04), ex);
                return BadRequest(ex.Message);
            }

        }
        /// <summary>
        /// Adds sub feature to the database also only allows uniquely named sub features
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost("Post")]
        [ProducesResponseType(typeof(SubFeatureModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromBody] SubFeatureModel model)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var detail = await _subFeatureManager.CreateAsync(_mapper.Map<Entities.SubFeature>(model));
                return Ok(_mapper.Map<SubFeatureModel>(detail));
            }
            catch (Exception ex)
            {
                _logger.LogError(_messageManager.GetMessage(MessageKeys.SF_MSG_05), ex);
                return BadRequest(ex.Message);
            }

        }
        /// <summary>
        /// Removes the sub feature from the database by taking the id of the subfeature as parameter
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("Delete/{id}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue || id.Value <= 0)
                return BadRequest(_messageManager.GetMessage(MessageKeys.SF_MSG_02));
            try
            {
                var isSuccess = await _subFeatureManager.DeleteAsync(id.Value);
                if (isSuccess)
                    return Ok(_messageManager.GetMessage(MessageKeys.SF_MSG_06));
                return BadRequest(_messageManager.GetMessage(MessageKeys.SF_MSG_07));
            }
            catch (Exception ex)
            {
                _logger.LogError(_messageManager.GetMessage(MessageKeys.SF_MSG_08), ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
