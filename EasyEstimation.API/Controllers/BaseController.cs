﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace EasyEstimation.API.Controllers
{
    [Route("api/[controller]"), ApiController, Consumes("application/json")]
    [Produces("application/json")]
    public class BaseController : ControllerBase
    {
    }
}
