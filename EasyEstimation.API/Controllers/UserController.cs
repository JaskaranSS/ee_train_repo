﻿using AutoMapper;
using EasyEstimation.API.Models;
using EasyEstimation.API.Resources;
using EasyEstimation.API.Resources.Abstracts;
using EasyEstimation.Services.Abstracts;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasyEstimation.API.Controllers
{
    public class UserController : BaseController
    {
        private readonly ILogger _logger;
        private readonly IMapper _mapper;
        private readonly IUserManager _userManager;
        private readonly IMessageManager _messageManager;

        public UserController(ILogger<UserController> logger,IMapper mapper,
            IUserManager userManager, IMessageManager messageManager)
        {
            _logger = logger;
            _mapper = mapper;
            _userManager = userManager;
            _messageManager = messageManager;
        }

        [HttpGet("Get")]
        [ProducesResponseType(typeof(IEnumerable<UserModel>), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get()
        {
            try
            {
                var users = await _userManager.GetAllAsync();

                return Ok(_mapper.Map<IEnumerable<UserModel>>(users));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, _messageManager.GetMessage(MessageKeys.US_MSG_01));
                return BadRequest(ex.Message);
            }

        }

        [HttpGet("{id}")]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> Get(int? id)
        {
            if (!id.HasValue || id.Value <= 0)
            {
                return BadRequest(_messageManager.GetMessage(MessageKeys.US_MSG_02));
            }
            try
            {
                var user = await _userManager.GetAsync(id.Value);

                return Ok(_mapper.Map<UserModel>(user));

            }
            catch (Exception ex)
            {
                _logger.LogError(ex, _messageManager.GetMessage(MessageKeys.US_MSG_03));
                return BadRequest(ex.Message);
            }

        }

        [HttpPut("Put")]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> Put([FromBody] UserModel model)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var detail = await _userManager.EditAsync(_mapper.Map<Entities.User>(model));
                return Ok(_mapper.Map<UserModel>(detail));
            }
            catch (Exception ex)
            {
                _logger.LogError(_messageManager.GetMessage(MessageKeys.US_MSG_04), ex);
                return BadRequest(ex.Message);
            }

        }

        [HttpPost("Post")]
        [ProducesResponseType(typeof(UserModel), StatusCodes.Status200OK)]
        public async Task<IActionResult> Post([FromBody] UserModel model)
        {

            if (!ModelState.IsValid)
                return BadRequest(ModelState);

            try
            {
                var detail = await _userManager.CreateAsync(_mapper.Map<Entities.User>(model));
                return Ok(_mapper.Map<UserModel>(detail));
            }
            catch (Exception ex)
            {
                _logger.LogError(_messageManager.GetMessage(MessageKeys.US_MSG_05), ex);
                return BadRequest(ex.Message);
            }

        }

        [HttpDelete("Delete/{id}")]
        [ProducesResponseType(typeof(string), StatusCodes.Status200OK)]
        public async Task<IActionResult> Delete(int? id)
        {
            if (!id.HasValue || id.Value <= 0)
                return BadRequest(_messageManager.GetMessage(MessageKeys.US_MSG_02));
            try
            {
                var isSuccess = await _userManager.DeleteAsync(id.Value);
                if (isSuccess)
                    return Ok(_messageManager.GetMessage(MessageKeys.US_MSG_06));
                return BadRequest(_messageManager.GetMessage(MessageKeys.US_MSG_07));
            }
            catch (Exception ex)
            {
                _logger.LogError(_messageManager.GetMessage(MessageKeys.US_MSG_08), ex);
                return BadRequest(ex.Message);
            }
        }
    }
}
