﻿using System.Collections.Generic;

namespace EasyEstimation.API.Models
{
    public class SubFeatureModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        public string Description { get; set; }
    }
}
