﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyEstimation.API.Models
{
    public class ImageModel
    {
        public int Id { get; set; }

        public string FormFile { get; set; }
    }
}
