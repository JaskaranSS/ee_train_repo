﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyEstimation.Web.Models
{
    public class ImageUploadModel
    {
        public int Id { get; set; }

        public IFormFile FormFile { get; set; }
    }
}
