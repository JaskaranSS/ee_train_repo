﻿namespace EasyEstimation.Web.Resources
{
    /// <summary>
    /// The class conatins the keys for the message mentioned in Messages.resx file
    /// </summary>
    public static class MessageKeys
    {
        #region Message for User
        public const string US_MSG_01 = "US_MSG_01";
        public const string US_MSG_02 = "US_MSG_02";
        public const string US_MSG_03 = "US_MSG_03";
        public const string US_MSG_04 = "US_MSG_04";
        public const string US_MSG_05 = "US_MSG_05";
        public const string US_MSG_06 = "US_MSG_06";
        public const string US_MSG_07 = "US_MSG_07";
        public const string US_MSG_08 = "US_MSG_08";

        #endregion

        #region Message for SubFeature
        public const string SF_MSG_01 = "SF_MSG_01";
        public const string SF_MSG_02 = "SF_MSG_02";
        public const string SF_MSG_03 = "SF_MSG_03";
        public const string SF_MSG_04 = "SF_MSG_04";
        public const string SF_MSG_05 = "SF_MSG_05";
        public const string SF_MSG_06 = "SF_MSG_06";
        public const string SF_MSG_07 = "SF_MSG_07";
        public const string SF_MSG_08 = "SF_MSG_08";

        #endregion

    }
}
