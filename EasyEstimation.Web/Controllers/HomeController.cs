﻿using EasyEstimation.Web.Models;
using EasyEstimation.Web.Resources;
using EasyEstimation.Web.Resources.Abstracts;
using EasyEstimation.Web.Services.Abstracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace EasyEstimation.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMessageManager _messageManager;
        private readonly IAdminService _adminService;

        public HomeController(ILogger<HomeController> logger, IMessageManager messageManager, IAdminService adminService)
        {
            _logger = logger;
            _messageManager = messageManager;
            _adminService = adminService;
        }

        public IActionResult Index()
        {
            _logger.LogInformation("Index function gets all users");
            _messageManager.GetMessage(MessageKeys.US_MSG_01);

            var allUsers = _adminService.GetUsers().Result;

            return View(allUsers);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
