﻿using AspNetCoreHero.ToastNotification.Abstractions;
using EasyEstimation.Web.Models;
using EasyEstimation.Web.Resources;
using EasyEstimation.Web.Resources.Abstracts;
using EasyEstimation.Web.Services.Abstracts;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System.Diagnostics;
using System.Threading.Tasks;
using EasyEstimation.Utilities;

namespace EasyEstimation.Web.Controllers
{
    public class SubFeatureController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly IMessageManager _messageManager;
        private readonly ISubFeatureService _SubFeatureService;
        private readonly INotyfService _notyf;

        public SubFeatureController(ILogger<HomeController> logger, IMessageManager messageManager, ISubFeatureService SubFeatureService, INotyfService notyf)
        {
            _logger = logger;
            _messageManager = messageManager;
            _SubFeatureService = SubFeatureService;
            _notyf = notyf;
        }

        public IActionResult Index()
        {
            _messageManager.GetMessage(MessageKeys.SF_MSG_01);
           
            var allSubFeatures = _SubFeatureService.GetSubFeatures().Result;
            
            return View("SubFeatureView", allSubFeatures);
        }

        public IActionResult AddSubFeature(SubFeatureModel model) {
            _logger.LogInformation("SubFeature is being added");
            _SubFeatureService.AddSubFeature(model);
            var allSubFeatures = _SubFeatureService.GetSubFeatures().Result;
            _notyf.Success("Record Added Successfully");
            return View("SubFeatureView", allSubFeatures);
        }
        [HttpGet("[action]")]
        public IActionResult AddRedirect()
        {
            return View("SubFeatureActionView");
        }
        public IActionResult UploadRedirect()
        {
            return View("SubFeatureFileUploadView");
        }

        [HttpGet("[action]")]
        public IActionResult EditRedirect() {            
               return View("SubFeatureEditView");
        }

        public IActionResult EditSubFeature(SubFeatureModel model) {

            _SubFeatureService.ModifySubFeature(model);
            var allSubFeatures = _SubFeatureService.GetSubFeatures().Result;
            _notyf.Success("Record Edited Successfully");
            return View("SubFeatureView", allSubFeatures);
        }
        public IActionResult DeleteSubFeature(int id) {
            _SubFeatureService.DeleteSubFeatureById(id);
            var allSubFeatures = _SubFeatureService.GetSubFeatures().Result;
            _notyf.Success("Record Deleted Successfully");
            return View("SubFeatureView", allSubFeatures);
        }
        public async Task<IActionResult> FileUpload(ImageUploadModel model) {
            var File = model.FormFile;
            string path = await ImageSaver.Saver(File, "SubFeatureImages");
            return View("SubFeatureFileUploadView");
        }
        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
