﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyEstimation.Web.HttpClients.Abstracts
{
    public interface IRestHttpClient
    {
        Task<TOutput> GetAsync<TOutput>(string requestUri, Dictionary<string, string> additionalHeaders = null);
        Task<TOutput> PostAsync<TInput, TOutput>(string requestUri, TInput request, Dictionary<string, string> additionalHeaders = null);
        Task<TOutput> DeleteAsync<TOutput>(string requestUri, Dictionary<string, string> additionalHeaders = null);
        Task<TOutput> PutAsync<TInput, TOutput>(string requestUri, TInput request, Dictionary<string, string> additionalHeaders = null);
    }
}
