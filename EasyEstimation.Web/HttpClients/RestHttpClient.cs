﻿using EasyEstimation.Utilities.Constants;
using EasyEstimation.Web.HttpClients.Abstracts;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace EasyEstimation.Web.HttpClients
{
    public class RestHttpClient : IRestHttpClient
    {
        private readonly HttpClient _client;
        private readonly ILogger<RestHttpClient> _logger;

        /// <summary>
        /// Initializes a new instance of the RestHttpClient
        /// </summary>
        /// <param name="httpClient"></param>
        public RestHttpClient(HttpClient httpClient, ILogger<RestHttpClient> logger)
        {
            _client = httpClient ?? throw new ArgumentNullException(nameof(httpClient));
            _logger = logger;
        }

        /// <summary>
        /// Generic method helps in calling DELETE API call
        /// </summary>
        /// <typeparam name="TOutput">Type of output</typeparam>
        /// <param name="requestUri">The requested URL</param>
        /// <param name="additionalHeaders">Header information</param>
        /// <returns></returns>
        public async Task<TOutput> DeleteAsync<TOutput>(string requestUri, Dictionary<string, string> additionalHeaders = null)
        {
            try
            {
                using HttpResponseMessage httpResponse = await _client.DeleteAsync(requestUri);
                if (httpResponse.IsSuccessStatusCode)
                {
                    //When no record is found
                    if (httpResponse.StatusCode == HttpStatusCode.NoContent)
                    {
                        return default;
                    }
                    //When one or more record are found
                    else if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        using HttpContent content = httpResponse.Content;
                        return await content.ReadFromJsonAsync<TOutput>();
                    }
                }
                return default;
            }
            catch (Exception)
            {
                _logger.LogError($"Exception while calling DELETE aync uri {requestUri}");
                throw;
            }
        }

        /// <summary>
        /// Generic method helps in calling GET API call
        /// </summary>
        /// <typeparam name="TOutput">Type of output</typeparam>
        /// <param name="requestUri">The requested URL</param>
        /// <param name="additionalHeaders">Header information</param>
        /// <returns></returns>
        public async Task<TOutput> GetAsync<TOutput>(string requestUri, Dictionary<string, string> additionalHeaders = null)
        {
            try
            {
                using HttpResponseMessage httpResponse = await _client.GetAsync(requestUri);
                if (httpResponse.IsSuccessStatusCode)
                {
                    //When no record is found
                    if (httpResponse.StatusCode == HttpStatusCode.NoContent)
                    {
                        return default;
                    }
                    //When one or more record are found
                    else if (httpResponse.StatusCode == HttpStatusCode.OK)
                    {
                        using HttpContent content = httpResponse.Content;
                        return await content.ReadFromJsonAsync<TOutput>();
                    }
                }
                return default;
            }
            catch (Exception)
            {
                _logger.LogError($"Exception while calling GET aync uri {requestUri}");
                throw;
            }
        }

        /// <summary>
        /// Generic method helps in calling POST API call
        /// </summary>
        /// <typeparam name="TInput">Type of input</typeparam>
        /// <typeparam name="TOutput">Type of output</typeparam>
        /// <param name="requestUri">The requested URL</param>
        /// <param name="request">The request body</param>
        /// <param name="additionalHeaders">Header information</param>
        /// <returns></returns>
        public async Task<TOutput> PostAsync<TInput, TOutput>(string requestUri, TInput request, Dictionary<string, string> additionalHeaders = null)
        {
            try
            {
                var jsonContent = JsonSerializer.Serialize(request);
                var stringContent = new StringContent(jsonContent, Encoding.UTF8, MediaType.ApplicationJson);                
                using HttpResponseMessage httpResponse = await _client.PostAsync(requestUri, stringContent);
                if (httpResponse.IsSuccessStatusCode)
                {
                    //When no record is found
                    if (httpResponse.StatusCode == HttpStatusCode.NoContent)
                    {
                        return default;
                    }
                    //When one or more record are found
                    else if (httpResponse.StatusCode is HttpStatusCode.OK or HttpStatusCode.Created)
                    {
                        using HttpContent content = httpResponse.Content;
                        return await content.ReadFromJsonAsync<TOutput>();
                    }
                }
                return default;
            }
            catch (Exception)
            {
                _logger.LogError($"Exception while calling POST aync uri {requestUri}");
                throw;
            }
        }

        /// <summary>
        ///  Generic method helps in calling PUT API call
        /// </summary>
        /// <typeparam name="TInput">Type of input</typeparam>
        /// <typeparam name="TOutput">Type of output</typeparam>
        /// <param name="requestUri">The requested URL</param>
        /// <param name="request">The request body</param>
        /// <param name="additionalHeaders">Header information</param>
        /// <returns></returns>
        public async Task<TOutput> PutAsync<TInput, TOutput>(string requestUri, TInput request, Dictionary<string, string> additionalHeaders = null)
        {
            try
            {
                var jsonContent = JsonSerializer.Serialize(request);
                var stringContent = new StringContent(jsonContent, Encoding.UTF8, MediaType.ApplicationJson);
                using HttpResponseMessage httpResponse = await _client.PutAsync(requestUri, stringContent);
                if (httpResponse.IsSuccessStatusCode)
                {
                    //When no record is found
                    if (httpResponse.StatusCode == HttpStatusCode.NoContent)
                    {
                        return default;
                    }
                    //When one or more record are found
                    else if (httpResponse.StatusCode is HttpStatusCode.OK or HttpStatusCode.Accepted)
                    {
                        using HttpContent content = httpResponse.Content;
                        return await content.ReadFromJsonAsync<TOutput>();
                    }
                }
                return default;
            }
            catch (Exception)
            {
                _logger.LogError($"Exception while calling PUT aync uri {requestUri}");
                throw;
            }
        }
    }
}
