﻿using EasyEstimation.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasyEstimation.Web.Services.Abstracts
{
    public interface ISubFeatureService
    {
        #region SubFeature Management

        /// <summary>
        /// Add new SubFeature
        /// </summary>
        /// <param name="SubFeatureModel">SubFeature to add</param>
        /// <returns></returns>
        Task<SubFeatureModel> AddSubFeature(SubFeatureModel SubFeatureModel);

        /// <summary>
        /// Get all SubFeatures list
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<SubFeatureModel>> GetSubFeatures();

        /// <summary>
        /// Get SubFeature by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<SubFeatureModel> GetSubFeatureById(int id);

        /// <summary>
        /// Modify SubFeature
        /// </summary>
        /// <param name="SubFeatureModel">User information to modify</param>
        /// <returns></returns>
        Task<SubFeatureModel> ModifySubFeature(SubFeatureModel SubFeatureModel);
        #endregion

        Task<SubFeatureModel> DeleteSubFeatureById(int id);
    }
}
