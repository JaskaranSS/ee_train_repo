﻿using EasyEstimation.Web.Models;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasyEstimation.Web.Services.Abstracts
{
    public interface IAdminService
    {
        #region User Management

        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="userModel">User to add</param>
        /// <returns></returns>
        Task<UserModel> AddUsers(UserModel userModel);

        /// <summary>
        /// Get all users list
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<UserModel>> GetUsers();

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<UserModel> GetUsersById(int id);

        /// <summary>
        /// Modify User
        /// </summary>
        /// <param name="userModel">User information to modify</param>
        /// <returns></returns>
        Task<UserModel> ModifyUser(UserModel userModel);
        #endregion


    }
}
