﻿using EasyEstimation.Web.HttpClients.Abstracts;
using EasyEstimation.Web.Models;
using EasyEstimation.Web.Services.Abstracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasyEstimation.Web.Services
{
    /// <summary>
    /// Class to handle Admin Service reuests
    /// </summary>
    public class AdminService : IAdminService
    {
        private readonly IRestHttpClient _restHttpClient;

        /// <summary>
        /// Initalize AdminService Object
        /// </summary>
        /// <param name="options">Configuration for API</param>
        /// <param name="restHttpClient">Rest API handler</param>
        public AdminService(IRestHttpClient restHttpClient)
        {
            _restHttpClient = restHttpClient;            
        }

        /// <summary>
        /// Add new user
        /// </summary>
        /// <param name="userModel">User to add</param>
        /// <returns></returns>
        public async Task<UserModel> AddUsers(UserModel userModel)
        {
            return await _restHttpClient.PostAsync<UserModel, UserModel>($"User", userModel);
        }

        /// <summary>
        /// Get all users list
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<UserModel>> GetUsers()
        {
            return await _restHttpClient.GetAsync<IEnumerable<UserModel>>($"User/Get");
        }

        /// <summary>
        /// Get user by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<UserModel> GetUsersById(int id)
        {
            return await _restHttpClient.GetAsync<UserModel>($"User/{id}");
        }

        /// <summary>
        /// Modify User
        /// </summary>
        /// <param name="userModel">User information to modify</param>
        /// <returns></returns>
        public async Task<UserModel> ModifyUser(UserModel userModel)
        {
            return await _restHttpClient.PutAsync<UserModel, UserModel>($"User", userModel);
        }
    }
}
