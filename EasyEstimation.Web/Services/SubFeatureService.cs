﻿using EasyEstimation.Web.HttpClients.Abstracts;
using EasyEstimation.Web.Models;
using EasyEstimation.Web.Services.Abstracts;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace EasyEstimation.Web.Services
{
    /// <summary>
    /// Class to handle SubFeature Service requests
    /// </summary>
    public class SubFeatureService : ISubFeatureService
    {
        private readonly IRestHttpClient _restHttpClient;

        /// <summary>
        /// Initalize SubFeatureService Object
        /// </summary>
        /// <param name="options">Configuration for API</param>
        /// <param name="restHttpClient">Rest API handler</param>
        public SubFeatureService(IRestHttpClient restHttpClient)
        {
            _restHttpClient = restHttpClient;
        }

        /// <summary>
        /// Add new SubFeature
        /// </summary>
        /// <param name="SubFeatureModel">SubFeature to add</param>
        /// <returns></returns>
        public async Task<SubFeatureModel> AddSubFeature(SubFeatureModel SubFeatureModel)
        {
            return await _restHttpClient.PostAsync<SubFeatureModel, SubFeatureModel>($"SubFeature/Post", SubFeatureModel);
        }

        /// <summary>
        /// Get all SubFeatures list
        /// </summary>
        /// <returns></returns>
        public async Task<IEnumerable<SubFeatureModel>> GetSubFeatures()
        {
            return await _restHttpClient.GetAsync<IEnumerable<SubFeatureModel>>($"SubFeature/Get");
        }

        /// <summary>
        /// Get SubFeature by id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<SubFeatureModel> GetSubFeatureById(int id)
        {
            return await _restHttpClient.GetAsync<SubFeatureModel>($"SubFeature/{id}");
        }

        /// <summary>
        /// Modify SubFeature
        /// </summary>
        /// <param name="SubFeatureModel">SubFeature information to modify</param>
        /// <returns></returns>
        public async Task<SubFeatureModel> ModifySubFeature(SubFeatureModel SubFeatureModel)
        {
            return await _restHttpClient.PutAsync<SubFeatureModel, SubFeatureModel>($"SubFeature", SubFeatureModel);
        }

        public async Task<SubFeatureModel> DeleteSubFeatureById(int id)
        {
            return await _restHttpClient.DeleteAsync<SubFeatureModel>($"SubFeature/Delete/{id}");
        }
    }
}
