﻿using AspNetCoreHero.ToastNotification;
using EasyEstimation.Web.HttpClients;
using EasyEstimation.Web.HttpClients.Abstracts;
using EasyEstimation.Web.Resources;
using EasyEstimation.Web.Resources.Abstracts;
using EasyEstimation.Web.Services;
using EasyEstimation.Web.Services.Abstracts;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Collections.Generic;
using System.Globalization;
using System.Reflection;

namespace EasyEstimation.Web.Extensions
{
    public static class ServiceCollectionExtensions
    {
        public static IServiceCollection AddInfrastructureLayerServices(this IServiceCollection services, IConfiguration Configuration)
        {
            services.AddDistributedMemoryCache();
            services.ConfigureLocalization();

            services.RegisterServices(Configuration);
            return services;
        }

        private static IServiceCollection ConfigureLocalization(this IServiceCollection services)
        {
            services.AddLocalization(options => options.ResourcesPath = "Resources");

            services.Configure<RequestLocalizationOptions>(
                       options =>
                       {
                           var supportedCultures = new List<CultureInfo>
                               {
                                   new CultureInfo("en-US"),
                               };

                           options.DefaultRequestCulture = new RequestCulture(culture: "en-US", uiCulture: "en-US");
                           options.SupportedCultures = supportedCultures;
                           options.SupportedUICultures = supportedCultures;
                           options.RequestCultureProviders.Insert(0, new QueryStringRequestCultureProvider());
                       });

            services.AddMvc()
                    .AddViewLocalization()
                    .AddDataAnnotationsLocalization(options =>
                    {
                        options.DataAnnotationLocalizerProvider = (type, factory) =>
                        {
                            var assemblyName = new AssemblyName(typeof(MessagesResource).GetTypeInfo().Assembly.FullName);
                            return factory.Create("MessagesResource", assemblyName.Name);
                        };
                    });

            return services;
        }


        private static IServiceCollection RegisterServices(this IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IMessageManager, MessageManager>();

            //Get base address for API
            var endpoint = configuration.GetSection("ApiEndPoints").GetSection("BaseAddress")?.Value;
            services.AddHttpClient<IRestHttpClient, RestHttpClient>(client => client.BaseAddress = new System.Uri(endpoint));

            services.AddNotyf(config => { config.DurationInSeconds = 10; config.IsDismissable = true; config.Position = NotyfPosition.BottomRight; });

            services.AddScoped<IAdminService, AdminService>();
            services.AddScoped<ISubFeatureService, SubFeatureService>();
            return services;
        }
    }
}
