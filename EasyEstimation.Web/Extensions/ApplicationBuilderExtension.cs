﻿using EasyEstimation.Web.Middlewares;
using Microsoft.AspNetCore.Builder;

namespace EasyEstimation.Web.Extensions
{
    public static class ApplicationBuilderExtension
    {
        public static IApplicationBuilder UseInfrastructureApplicationBuilder(this IApplicationBuilder app)
        {
            app.UseRouting();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
            app.UseGlobalErrorHandler();
            app.UseAuthorization();
            return app;
        }

        private static IApplicationBuilder UseGlobalErrorHandler(this IApplicationBuilder app)
        {
            app.UseMiddleware<GlobalErrorHandler>();
            return app;
        }

    }
}
