﻿namespace EasyEstimation.Entities
{
    public class CategoryFeature : BaseEntity<int>
    {
        public int CategoryId { get; set; }
        public int FeatureId { get; set; }
    }
}
