﻿namespace EasyEstimation.Entities
{
    public class FeatureDetail : BaseEntity<int>
    {
        public int FeatureId { get; set; }
        public string Name { get; set; }
        public int? ParentFeatureDetailId { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }
    }
}
