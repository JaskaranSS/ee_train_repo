﻿using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyEstimation.Entities
{
    public class ImageModel: BaseEntity<int>
    {
        public IFormFile FormFile { get; set; }
    }
}
