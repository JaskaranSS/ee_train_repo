﻿using System;
using System.Collections.Generic;

namespace EasyEstimation.Entities
{
    public class UserEstimation : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Mobile { get; set; }
        public DateTime CreatedAt { get; set; }
        public string OrganisationName { get; set; }
        public virtual ICollection<UserEstimationDetail> UserEstimationDetails { get; set; }
    }
}
