﻿using System.Collections.Generic;

namespace EasyEstimation.Entities
{
    public class SubFeature : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public ImageModel Image { get; set; }
    }
}