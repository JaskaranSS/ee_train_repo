﻿namespace EasyEstimation.Entities
{
    public class Category : BaseEntity<int>
    {
        public string Name { get; set; }
        public bool IsActive { get; set; }
    }
}
