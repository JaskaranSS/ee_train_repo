﻿namespace EasyEstimation.Entities
{
    public class Feature : BaseEntity<int>
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string Icon { get; set; }
        public bool IsActive { get; set; }
    }
}
