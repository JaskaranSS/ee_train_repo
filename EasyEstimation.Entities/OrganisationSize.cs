﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EasyEstimation.Entities
{
    public class OrganisationSize : BaseEntity<int>
    {
        public string Name { get; set; }
    }
}
