﻿namespace EasyEstimation.Entities
{
    public class PricingDetail : BaseEntity<int>
    {
        public int CategoryId { get; set; }
        public double Rate { get; set; }
        public bool IsActive { get; set; }
    }
}
