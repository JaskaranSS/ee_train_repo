﻿using Microsoft.AspNetCore.Http;
using System;
using System.IO;
using System.Threading.Tasks;

namespace EasyEstimation.Utilities
{
    /// <summary>
    /// Utility for saving image files to server folder wwwroot folder
    /// takes the file and the destination folder inside wwwroot as parameters
    /// In case of error will return empty string
    /// </summary>
    static public class ImageSaver
    {
        public static async Task<string> Saver(IFormFile file, string folder)
        {
            if (!(file.Length > 0 && (folder.Equals("SubFeatureImages") || folder.Equals("IndustryImages")|| folder.Equals("FeatureImages")
                || folder.Equals("ProjectImages"))))
            {
                return string.Empty;
            }
            string fileName = Convert.ToString(DateTime.Now).Replace(":", string.Empty).Replace("-", string.Empty);
            fileName = fileName + Convert.ToString(Path.GetExtension(Path.GetFileName(file.FileName)));
            var filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot", "images", folder, fileName);
            using (FileStream stream = System.IO.File.Create(filePath))
            {
                await file.CopyToAsync(stream);
                stream.Flush();
            }
            return filePath;
            }
        }
    }
