﻿namespace EasyEstimation.Utilities.Constants
{
    /// <summary>
    /// This class represents the contant related to Media Type for http request and response
    /// </summary>
    public static class MediaType
    {
        /// <summary>
        /// Reponse type JSON
        /// </summary>
        public const string ApplicationJson = "application/json";
    }
}
